# 1era Vez

# instalar git 
    sudo apt-get install git

# instalar requerimientos del sistema
    sudo apt-get install build-essential libssl-dev libffi-dev python-dev


# instalar el módulo venv para python3
    sudo apt-get install -y python3-venv    

# crear un directorio, donde poner le proyecto 
    mkdir proy

# clonar el proyecto
    git clone https://charleipn@bitbucket.org/charleipn/tutorias.git

# entrar y crear directorio donde poner el ambiente virtual
    cd tutorias
    mkdir tmp/

# crear el ambiente virtual
    python3 -m venv tmp/venv
    
# activar el ambiente virtual
    source tmp/venv/bin/activate

# instalar los requirements
    pip3 install -r etc/requirements.txt
    
# Entrar al proyecto app
    cd app
# Aplica migraciones y crea SU
    ./manage.py migrate
    ./manage.py createsuperuser

# Corre el servidor local
    ./manage.py runserver
    
    
# LISTO ENTA AL SISTEMA COMO ADMIN  en localhost:8000/  con el correo y contraseña que pusiste en el superuser


#######################################################################################################################################
# Diario para desarrollar


# Cada vez que inicie el proyecto
    cd proy/turorias/
    
# activar el ambiente virtual
    source tmp/venv/bin/activate

# Entrar al proyecto app
    cd app
    
# Corre el servidor local
    ./manage.py runserver



