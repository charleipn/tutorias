from django.urls import path, include, re_path

from .views import Index

app_name = 'panel_teacher'

urlpatterns = [
    path('', Index.as_view(), name="index"),
    path('tutorias/', include('panel_teacher.modules.tutorias.urls', namespace="tutorias")),
]
