from datetime import datetime
from django import forms
from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from operator import itemgetter
from django.db.models import Q

from teachers.models import Teacher
from students.models import Student

from tutors.models import (StudentTutoriasIndividual, StudentTutoriasRegularizacion,
    StudentTutoriasAcademica, StudentTutoriasPares, Note)

from django.views.generic import (
    TemplateView, RedirectView, FormView,
    CreateView, DetailView, UpdateView, DeleteView, ListView
)

# Filtro
PAGINATED = 50
from django_filters.views import FilterView
from tutors.filters import (TutoriasIndividualFilter, TutoriasRegularizacionFilter,
    TutoriasAcademicaFilter, TutoriasParesFilter)



class BaseView(LoginRequiredMixin):
    template_dir = "panel_teacher/tutorias/"

    def get_teacher(self):
        return self.request.user.user_teacher if self.request.user.user_teacher else None

class Index(BaseView, TemplateView):
    template_name = BaseView.template_dir + 'index.html'


class IndexIndividual(BaseView, FilterView):
    model = StudentTutoriasIndividual
    filterset_class = TutoriasIndividualFilter
    template_name = BaseView.template_dir + 'individual/index.html'
    paginate_by=PAGINATED

    def get_queryset(self):
        teacher = BaseView.get_teacher(self)
        qs = StudentTutoriasIndividual.objects.filter(teacher_tutor = teacher)
        filtered = TutoriasIndividualFilter(self.request.GET, queryset=qs)
        return filtered.qs

class DetailIndividual(BaseView, DetailView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/detail.html'

class UpdateIndividual(BaseView, UpdateView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/update.html'
    fields = ['start', 'end', 'location']

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-individual', kwargs={'pk': self.kwargs["pk"]})

class FinishIndividual(BaseView, UpdateView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/finish.html'
    fields = []

    def form_valid(self, form):
        form.instance.is_finished = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-individual', kwargs={'pk': self.kwargs["pk"]})

class NoteIndividual(BaseView, CreateView):
    model = Note
    template_name = BaseView.template_dir + 'individual/note.html'
    fields = ['content']

    def form_valid(self, form):
        individual = self.get_StudentTutoriasIndividual()
        form.instance.tutorias_individual = individual
        return super().form_valid(form)

    def get_StudentTutoriasIndividual(self):
        return StudentTutoriasIndividual.objects.get(pk=self.kwargs["pk"])


    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-individual', kwargs={'pk': self.kwargs["pk"]})


class IndexRegularizacion(BaseView, FilterView):
    model = StudentTutoriasRegularizacion
    filterset_class = TutoriasRegularizacionFilter
    template_name = BaseView.template_dir + 'regularizacion/index.html'
    paginate_by=PAGINATED

    def get_queryset(self):
        teacher = BaseView.get_teacher(self)
        qs = StudentTutoriasRegularizacion.objects.filter(teacher_tutor = teacher)
        filtered = TutoriasRegularizacionFilter(self.request.GET, queryset=qs)
        return filtered.qs

class DetailRegularizacion(BaseView, DetailView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/detail.html'

class UpdateRegularizacion(BaseView, UpdateView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/update.html'
    fields = ['start', 'end', 'location']

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-regularizacion', kwargs={'pk': self.kwargs["pk"]})

class FinishRegularizacion(BaseView, UpdateView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/finish.html'
    fields = []

    def form_valid(self, form):
        form.instance.is_finished = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-regularizacion', kwargs={'pk': self.kwargs["pk"]})

class NoteRegularizacion(BaseView, CreateView):
    model = Note
    template_name = BaseView.template_dir + 'regularizacion/note.html'
    fields = ['content']

    def form_valid(self, form):
        regularizacion = self.get_StudentTutoriasRegularizacion()
        form.instance.tutorias_regularizacion = regularizacion
        return super().form_valid(form)

    def get_StudentTutoriasRegularizacion(self):
        return StudentTutoriasRegularizacion.objects.get(pk=self.kwargs["pk"])

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-regularizacion', kwargs={'pk': self.kwargs["pk"]})


class IndexAcademica(BaseView, FilterView):
    model = StudentTutoriasAcademica
    filterset_class = TutoriasAcademicaFilter
    template_name = BaseView.template_dir + 'academica/index.html'
    paginate_by = PAGINATED

    def get_queryset(self):
        teacher = BaseView.get_teacher(self)
        print(teacher)
        qs = StudentTutoriasAcademica.objects.filter(teacher_tutor = teacher)
        filtered = TutoriasAcademicaFilter(self.request.GET, queryset=qs)
        return filtered.qs

class DetailAcademica(BaseView, DetailView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/detail.html'

class UpdateAcademica(BaseView, UpdateView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/update.html'
    fields = ['start', 'end', 'location']

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-academica', kwargs={'pk': self.kwargs["pk"]})


class FinishAcademica(BaseView, UpdateView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/finish.html'
    fields = []

    def form_valid(self, form):
        form.instance.is_finished = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-academica', kwargs={'pk': self.kwargs["pk"]})

class NoteAcademica(BaseView, CreateView):
    model = Note
    template_name = BaseView.template_dir + 'academica/note.html'
    fields = ['content']

    def form_valid(self, form):
        academica = self.get_StudentTutoriasAcademica()
        form.instance.tutorias_academica = academica
        return super().form_valid(form)

    def get_StudentTutoriasAcademica(self):
        return StudentTutoriasAcademica.objects.get(pk=self.kwargs["pk"])

    def get_success_url(self):
        return reverse('panel_teacher:tutorias:detail-academica', kwargs={'pk': self.kwargs["pk"]})
