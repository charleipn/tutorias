from django.apps import AppConfig


class PanelTeacherConfig(AppConfig):
    name = 'panel_teacher'
