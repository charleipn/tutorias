from django.db import models
from app.models import DateTimeStampModel, PersonModel
from django.contrib.auth.models import User

# Create your models here.

class Admistrator(DateTimeStampModel, PersonModel):
    class Meta:
        verbose_name = "administrador"
        verbose_name_plural = "Administradores"

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user_admin")

    def __str__(self):
        return "{} {}".format(self.name, self.last_name)

    def full_name(self):
        return '{} {}'.format(self.name, self.last_name)
