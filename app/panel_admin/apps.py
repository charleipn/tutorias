from django.apps import AppConfig


class PanelAdminConfig(AppConfig):
    name = 'panel_admin'
