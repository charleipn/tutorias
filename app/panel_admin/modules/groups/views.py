from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse

from tutors.models import GroupTutorias

from django.views.generic import (FormView, DetailView, UpdateView, ListView, CreateView)

# Filtro
PAGINATED = 50
from django_filters.views import FilterView
from tutors.filters import (GroupsTutoriasFilter)


class BaseView(LoginRequiredMixin):
    model = GroupTutorias
    template_dir = "panel_admin/groups/"
    pk_url_kwarg = 'group_pk'

    def get_queryset(self):
        qs = GroupTutorias.objects.all()
        return qs

class Index(BaseView, FilterView):
    filterset_class = GroupsTutoriasFilter
    template_name = BaseView.template_dir + "index.html"
    paginate_by = PAGINATED

    def get_queryset(self):
        qs = GroupTutorias.objects.all()
        filtered = GroupsTutoriasFilter(self.request.GET, queryset=qs)
        return filtered.qs


class Create(BaseView, CreateView):
    template_name = BaseView.template_dir + "create.html"
    fields = ["group", 'period_school', 'teacher', 'start', 'end']

    def get_success_url(self):
        return reverse('panel_admin:groups:index')


class Update(BaseView, UpdateView):
    template_name = BaseView.template_dir + "update.html"
    fields = ["group", 'period_school', 'teacher', 'start', 'end']

    def get_success_url(self):
        return reverse('panel_admin:groups:detail', kwargs = {'group_pk': self.object.pk})


class Detail(BaseView, DetailView):
    template_name = BaseView.template_dir + "detail.html"
