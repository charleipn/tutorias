from django.urls import path, include

from .views import Index, Create, Detail, Update

app_name = "students"

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('nuevo/', Create.as_view(), name='create'),
    path('<student_pk>/', Detail.as_view(), name='detail'),
    path('<student_pk>/editar', Update.as_view(), name='update'),
]
