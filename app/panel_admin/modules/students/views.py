from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse

from students.models import Student
from app.forms import StudentForm
from app.services import create_user

from django.views.generic import (FormView, DetailView, UpdateView, ListView)

# Filtro
PAGINATED = 50
from django_filters.views import FilterView
from students.filters import (StudentFilter)


class BaseView(LoginRequiredMixin):
    model = Student
    template_dir = "panel_admin/students/"
    pk_url_kwarg = 'student_pk'

    def get_queryset(self):
        qs = Student.objects.all()
        return qs

class Index(BaseView, FilterView):
    filterset_class = StudentFilter
    template_name = BaseView.template_dir + "index.html"
    paginate_by = PAGINATED

    def get_queryset(self):
        qs = Student.objects.all()
        filtered = StudentFilter(self.request.GET, queryset=qs)
        return filtered.qs


class Create(BaseView, FormView):
    template_name = BaseView.template_dir + "create.html"
    form_class = StudentForm

    def get_success_url(self):
        return reverse('panel_admin:students:index')

    def form_valid(self, form):

        email = form.cleaned_data['email']
        password = form.cleaned_data['password1']
        user = create_user(email, password)

        name = form.cleaned_data['name']
        last_name = form.cleaned_data['last_name']
        boleta = form.cleaned_data["boleta"]

        student = Student(
            user = user,
            name =  name,
            last_name = last_name,
            boleta = boleta
        )
        student.save()

        return super().form_valid(form)

class Update(BaseView, UpdateView):
    template_name = BaseView.template_dir + "update.html"
    fields = ["boleta", "turno", 'is_tutor']

    def get_success_url(self):
        return reverse('panel_admin:students:detail', kwargs = {'student_pk': self.object.pk})


class Detail(BaseView, DetailView):
    template_name = BaseView.template_dir + "detail.html"
