import django_filters

from .models import (Student)

from app.catalogues import (TURNO_MATUTINO, TURNO_VESPERTINO, TURNO_CHOICE)

class StudentFilter(django_filters.FilterSet):
    name_icontains = django_filters.CharFilter(field_name='name', lookup_expr='icontains', label='Nombre')
    last_name_icontains = django_filters.CharFilter(field_name='last_name', lookup_expr='icontains', label='Apellidos')
    boleta_icontains = django_filters.CharFilter(field_name='boleta', lookup_expr='icontains', label='Boleta')
    tutor_iexact = django_filters.ChoiceFilter(field_name='is_tutor', choices=((0, 'No es Tutor'), (1, 'Es Tutor')), lookup_expr='iexact', label='Tutor')

    class Meta:
        model = Student
        fields = [
            'name_icontains',
            'last_name_icontains',
            'boleta_icontains',
            'tutor_iexact',
        ]
