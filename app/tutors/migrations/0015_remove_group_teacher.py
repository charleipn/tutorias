# Generated by Django 3.0.3 on 2020-06-15 02:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutors', '0014_auto_20200614_2040'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='teacher',
        ),
    ]
