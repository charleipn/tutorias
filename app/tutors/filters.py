import django_filters

from .models import (StudentTutoriasIndividual, StudentTutoriasRegularizacion, StudentTutoriasAcademica,
    StudentTutoriasPares, GroupTutorias, PeriodSchool)

class TutoriasIndividualFilter(django_filters.FilterSet):
    student__name_icontains = django_filters.CharFilter(field_name='student__name', lookup_expr='icontains', label='Nombre')
    student__last_name_icontains = django_filters.CharFilter(field_name='student__last_name', lookup_expr='icontains', label='Apellidos')
    student__boleta_icontains = django_filters.NumberFilter(field_name='student__boleta', lookup_expr='icontains', label='Boleta')

    class Meta:
        model = StudentTutoriasIndividual
        fields = [
            'student__name_icontains',
            'student__last_name_icontains',
            'student__boleta_icontains'
            ]

class TutoriasRegularizacionFilter(django_filters.FilterSet):
    student__name_icontains = django_filters.CharFilter(field_name='student__name', lookup_expr='icontains', label='Nombre')
    student__last_name_icontains = django_filters.CharFilter(field_name='student__last_name', lookup_expr='icontains', label='Apellidos')
    student__boleta_icontains = django_filters.NumberFilter(field_name='student__boleta', lookup_expr='icontains', label='Boleta')
    student__materias_icontains = django_filters.CharFilter(field_name='materia__name', lookup_expr='icontains', label='Materia')

    class Meta:
        model = StudentTutoriasRegularizacion
        fields = [
            'student__name_icontains',
            'student__last_name_icontains',
            'student__boleta_icontains',
            'student__materias_icontains'
            ]

class TutoriasAcademicaFilter(django_filters.FilterSet):
    student__name_icontains = django_filters.CharFilter(field_name='student__name', lookup_expr='icontains', label='Nombre')
    student__last_name_icontains = django_filters.CharFilter(field_name='student__last_name', lookup_expr='icontains', label='Apellidos')
    student__boleta_icontains = django_filters.NumberFilter(field_name='student__boleta', lookup_expr='icontains', label='Boleta')
    student__materias_icontains = django_filters.CharFilter(field_name='materia__name', lookup_expr='icontains', label='Materia')

    class Meta:
        model = StudentTutoriasAcademica
        fields = [
            'student__name_icontains',
            'student__last_name_icontains',
            'student__boleta_icontains',
            'student__materias_icontains'
            ]

class TutoriasParesFilter(django_filters.FilterSet):
    student__name_icontains = django_filters.CharFilter(field_name='student__name', lookup_expr='icontains', label='Nombre')
    student__last_name_icontains = django_filters.CharFilter(field_name='student__last_name', lookup_expr='icontains', label='Apellidos')
    student__boleta_icontains = django_filters.NumberFilter(field_name='student__boleta', lookup_expr='icontains', label='Boleta')
    student__materias_icontains = django_filters.CharFilter(field_name='materia__name', lookup_expr='icontains', label='Materia')

    class Meta:
        model = StudentTutoriasPares
        fields = [
            'student__name_icontains',
            'student__last_name_icontains',
            'student__boleta_icontains',
            'student__materias_icontains'
            ]

class GroupsTutoriasFilter(django_filters.FilterSet):
    name__icontains = django_filters.CharFilter(field_name='group__name', lookup_expr='icontains', label='Grupo')
    period_school__ixact = django_filters.ModelChoiceFilter(field_name='period_school__name', queryset=PeriodSchool.objects.all(), lookup_expr='iexact', label='Periodo Escolar')

    class Meta:
        model = GroupTutorias
        fields = [
            'name__icontains',
            'period_school__ixact'
        ]
