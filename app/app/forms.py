from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django import forms

from django.utils.text import capfirst
from django.utils.translation import gettext, gettext_lazy as _

from tutors.models import Materia, TypeTutorias

UserModel = get_user_model()


class EmailAuthenticationForm(forms.Form):
    """
    Replase base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    email = forms.EmailField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True}),
    )
    password = forms.CharField(
        label=("Password"),
        strip=False,
        widget=forms.PasswordInput,
    )

    error_messages = {
        'invalid_login': _(
            "Please enter a correct %(email)s and password. Note that both "
            "fields may be case-sensitive."
        ),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        # Set the label for the "username" field.
        self.email_field = UserModel._meta.get_field(UserModel.EMAIL_FIELD)
        # if self.fields['email'].label is None:
        #     self.fields['email'].label = capfirst(self.email_field.verbose_name)
        self.fields['email'].widget.attrs.update({'placeholder': "Usuario..."})
        self.fields['password'].widget.attrs.update({'placeholder': "Contraseña..."})

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email is not None and password:
            try:
                user = User.objects.get(email=email)
                username = user.username
            except:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'email': self.email_field.verbose_name},
                )

            self.user_cache = authenticate(self.request, username=username, password=password)

            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'email': self.email_field.verbose_name},
                )
            else:
                self.cleaned_data.update({
                    'username': username
                })
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class RegisterForm(forms.Form):
    name = forms.CharField(label='Nombre', max_length=128, label_suffix="Nombre 1")
    last_name = forms.CharField(label='Apellido', max_length=128)
    boleta = forms.CharField(label='Boleta', max_length=16)
    type_tutorias = forms.ModelChoiceField(label="Tipo de Tutoria", queryset=TypeTutorias.objects.all())
    materias = forms.ModelChoiceField(queryset=Materia.objects.all())
    email = forms.EmailField(label='Email', max_length=128)
    password1 = forms.CharField(label='Contraseña', max_length=64, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirme Contraseña', max_length=64, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = "Nombre"
        self.fields['last_name'].widget.attrs['placeholder'] = "Apellido"
        self.fields['type_tutorias'].widget.attrs['placeholder'] = "tipo de tutorias"
        self.fields['materias'].widget.attrs['placeholder'] = "Materias"
        self.fields['email'].widget.attrs['placeholder'] = "email"
        self.fields['password1'].widget.attrs['placeholder'] = "Contraseña"
        self.fields['password2'].widget.attrs['placeholder'] = "Repite contraseña"

    def clean_email(self):
        data = self.cleaned_data['email']

        user = User.objects.filter(email=data)
        if len(user) > 0:
            raise forms.ValidationError("El Email ya Existe")

        # Always return a value to use as the new cleaned data, even if
        # this method didn't change it.
        return data

    def clean(self):
        cleaned_data = super().clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")

        if password1 != password2:
            msg = "Las contraseñas no son iguales"
            self.add_error('password1', msg)
            self.add_error('password2', msg)


class TeacherForm(forms.Form):
    name = forms.CharField(label='Nombre', max_length=128)
    last_name = forms.CharField(label='Apellido', max_length=128)
    id_worker = forms.CharField(label='Número del trabajador', max_length=32)
    materias = forms.ModelMultipleChoiceField(queryset=Materia.objects.all())
    email = forms.EmailField(label='Email', max_length=128)

    password1 = forms.CharField(label='Contraseña', max_length=64, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirme Contraseña', max_length=64, widget=forms.PasswordInput)

    def clean_email(self):
        data = self.cleaned_data['email']

        user = User.objects.filter(email=data)
        if len(user) > 0:
            raise forms.ValidationError("El Email ya Existe")

        # Always return a value to use as the new cleaned data, even if
        # this method didn't change it.
        return data

    def clean(self):
        cleaned_data = super().clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")

        if password1 != password2:
            msg = "Las contraseñas no son iguales"
            self.add_error('password1', msg)
            self.add_error('password2', msg)


class StudentForm(forms.Form):
    name = forms.CharField(label='Nombre', max_length=128)
    last_name = forms.CharField(label='Apellido', max_length=128)
    boleta = forms.CharField(label='Boleta', max_length=16)
    email = forms.EmailField(label='Email', max_length=128)

    password1 = forms.CharField(label='Contraseña', max_length=64, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirme Contraseña', max_length=64, widget=forms.PasswordInput)

    def clean_email(self):
        data = self.cleaned_data['email']

        user = User.objects.filter(email=data)
        if len(user) > 0:
            raise forms.ValidationError("El Email ya Existe")

        # Always return a value to use as the new cleaned data, even if
        # this method didn't change it.
        return data

    def clean(self):
        cleaned_data = super().clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")

        if password1 != password2:
            msg = "Las contraseñas no son iguales"
            self.add_error('password1', msg)
            self.add_error('password2', msg)

    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = "Nombre"
        self.fields['last_name'].widget.attrs['placeholder'] = "Apellido"
        self.fields['boleta'].widget.attrs['placeholder'] = "Boleta"
        self.fields['email'].widget.attrs['placeholder'] = "Email"
        self.fields['password1'].widget.attrs['placeholder'] = "Contraseña"
        self.fields['password2'].widget.attrs['placeholder'] = "Repite contraseña"
