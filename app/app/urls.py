"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.contrib.auth import views as auth_views
from .views import login, Index, RegisterStudent

from django.views.generic import TemplateView, RedirectView

urlpatterns = [
    path('', RedirectView.as_view(url='/login/')),
    path('index/', Index.as_view(), name="index"),
    path('administrador/', include('panel_admin.urls', namespace='panel_admin')),
    path('profesor/', include('panel_teacher.urls', namespace='panel_teacher')),
    path('estudiante/', include('panel_student.urls', namespace='panel_student')),
    path('admin/', admin.site.urls),
	path('login/', login, name="login"),
    path('logout/', auth_views.logout_then_login, name="logout"),

    path('registro/', include([
        path('estudiante/', RegisterStudent.as_view(), name="register-student"),
        ])),
]
