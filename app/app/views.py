from django.contrib.auth.views import LoginView
from django.contrib.auth.views import PasswordResetView as PRV
from django.contrib.auth.forms import PasswordResetForm as PRF

from django.views.generic import TemplateView, RedirectView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse

from .forms import EmailAuthenticationForm, StudentForm

from tutors.models import TypeTutorias

from students.models import Student


class Index(LoginRequiredMixin, TemplateView):
    template_name = "webapp/index.html"

    def get_template_names(self):
        try:
            person = self.request.user.person
            if person:
                return ['webapp/index.html']
        except Exception as e:
            pass

        return [self.template_name]

    def dispatch(self, request, *args, **kwargs):
        user = self.get_user(request)
        print(user)
        if hasattr(user, 'user_student'):
            return redirect('/estudiante/')

        elif hasattr(user, 'user_teacher'):
            return redirect('/profesor/')

        elif not (hasattr(user, 'user_student') and hasattr(user, 'user_teacher')) or user.is_superuser:
            return redirect('/administrador/')

        return super().dispatch(request, *args, *kwargs)

    def get_user(self, request):
        user = getattr(request, 'user', None)
        return user


# Create your views here.
# login
class EmailLoginView(LoginView):
    form_class = EmailAuthenticationForm
    success_url = 'estudiantes/'

def login(request, *args, **kwargs):
    return EmailLoginView.as_view(**kwargs)(request, *args, **kwargs)

class RegisterStudent(FormView):
    form_class = StudentForm
    template_name = "registration/register.html"

    def form_valid(self, form):
        from app.services import create_user
        data = {
            "email": form.cleaned_data['email'],
            "password": form.cleaned_data['password1']
        }
        user = create_user(**data)

        # Create Student
        student = Student(
            user = user,
            name = form.cleaned_data['name'],
            last_name = form.cleaned_data['last_name'],
            boleta = form.cleaned_data['boleta']
        )
        student.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('login')
