from django.db import models
from app.models import DateTimeStampModel, PersonModel
from django.contrib.auth.models import User

from app.catalogues import (TURNO_MATUTINO, TURNO_VESPERTINO, TURNO_CHOICE)
# Create your models here.

class Teacher(DateTimeStampModel, PersonModel):
    class Meta:
        verbose_name = "Profesor"
        verbose_name_plural = "Profesores"

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user_teacher")

    materias = models.ManyToManyField('tutors.materia')
    turno = models.CharField(max_length=5, choices=TURNO_CHOICE, default=TURNO_MATUTINO)
    is_available = models.BooleanField('¿Está disponible?', default=True)
    id_worker = models.CharField('Número de trabajador', max_length=32,  null=True)

    def __str__(self):
        return "{} - {} {}".format(self.id_worker, self.name, self.last_name)

    @property
    def full_name(self):
        return '{} {}'.format(self.name, self.last_name)

    @property
    def materias_str(self):
        materias = ""
        for mat in self.materias.all():
            materias += mat.name + ', '

        return materias[:-2] #para eliminar el espacio y la coma del ultimo elemento
