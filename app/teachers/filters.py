import django_filters

from .models import (Teacher)

from app.catalogues import (TURNO_MATUTINO, TURNO_VESPERTINO, TURNO_CHOICE)

class TeacherFilter(django_filters.FilterSet):
    name_icontains = django_filters.CharFilter(field_name='name', lookup_expr='icontains', label='Nombre')
    last_name_icontains = django_filters.CharFilter(field_name='last_name', lookup_expr='icontains', label='Apellidos')
    materia_icontains = django_filters.CharFilter(field_name='materia', lookup_expr='icontains', label='Materia')
    turno_iexact = django_filters.ChoiceFilter(field_name='turno', choices=TURNO_CHOICE, lookup_expr='iexact', label='Turno')
    is_worker_icontains = django_filters.CharFilter(field_name='id_worker', lookup_expr='icontains', label='Número de trabajador')

    class Meta:
        model = Teacher
        fields = [
            'name_icontains',
            'last_name_icontains',
            'materia_icontains',
            'turno_iexact',
            'materia_icontains'
        ]
