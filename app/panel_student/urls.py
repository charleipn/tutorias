from django.urls import path, include, re_path

from .views import Index

app_name = 'panel_student'

urlpatterns = [
    path('', Index.as_view(), name="index"),
    path('tutorias/', include('panel_student.modules.tutorias.urls', namespace="tutorias")),
    path('mi-perfil/', include('panel_student.modules.profile.urls', namespace="profile"))

]
