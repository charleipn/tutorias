from django.urls import path, include

from .views import (Detail, Update)

app_name = "profile"

urlpatterns = [
    path('<pk>/', Detail.as_view(), name='detail'),
    path('<pk>/editar', Update.as_view(), name='update'),
]
