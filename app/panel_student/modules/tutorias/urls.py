from django.urls import path, include

from .views import (Index, IndexIndividual, DetailIndividual, CreateIndividual, UpdateIndividual,
    IndexRegularizacion, DetailRegularizacion, CreateRegularizacion, UpdateRegularizacion,
    IndexAcademica, DetailAcademica, CreateAcademica, UpdateAcademica,
    IndexPares, DetailPares, CreatePares, UpdatePares,)

app_name = "tutorias"

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('individual/', include([
        path('', IndexIndividual.as_view(), name="index-individual"),
        path('nueva/', CreateIndividual.as_view(), name="create-individual"),
        path('<pk>/', DetailIndividual.as_view(), name="detail-individual"),
        path('<pk>/editar/', UpdateIndividual.as_view(), name="update-individual")
    ])),
    path('regularizacion/', include([
        path('', IndexRegularizacion.as_view(), name="index-regularizacion"),
        path('nueva/', CreateRegularizacion.as_view(), name="create-regularizacion"),
        path('<pk>/', DetailRegularizacion.as_view(), name="detail-regularizacion"),
        path('<pk>/editar/', UpdateRegularizacion.as_view(), name="update-regularizacion")
    ])),
    path('academica/', include([
        path('', IndexAcademica.as_view(), name="index-academica"),
        path('nueva/', CreateAcademica.as_view(), name="create-academica"),
        path('<pk>/', DetailAcademica.as_view(), name="detail-academica"),
        path('<pk>/editar/', UpdateAcademica.as_view(), name="update-academica")
    ])),
    path('pares/', include([
        path('', IndexPares.as_view(), name="index-pares"),
        path('nueva/', CreatePares.as_view(), name="create-pares"),
        path('<pk>/', DetailPares.as_view(), name="detail-pares"),
        path('<pk>/editar/', UpdatePares.as_view(), name="update-pares")
    ])),
]
