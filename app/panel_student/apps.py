from django.apps import AppConfig


class PanelStudentConfig(AppConfig):
    name = 'panel_student'
